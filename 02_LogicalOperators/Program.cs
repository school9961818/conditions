/*
 * Podmínka vždy buď platí nebo neplatí, ale můžeme se v ní ptát na více věcí.
 * Používáme k tomu logické operátory (google nebo třeba - https://www.w3schools.com/cs/cs_operators_logical.php)
 * V následujícím příkladu vypíšeme, jestli může uživatel řídit na základě jeho věku a informaci, 
 * jestli má nebo nemá řidičák
 */

int age = 19;
bool hasDrivingLicence = true;

if (hasDrivingLicence)
{
    if (age >= 18)
    {
        Console.WriteLine("Can drive.");
    }
    else
    {
        Console.WriteLine("Can't drive.");
    }
}
else
{
    Console.WriteLine("Can't drive.");
}

/*
* Můžete změnit vstupní hodnoty (např. věk = 15) a ověřit funkčnost kódu.
* Zároveň si všimněte, že stejný příkaz používáme dvakrát (Can't drive)
* Logické operátory nám umožní kód zjednodušit.
*/
/*
if (hasDrivingLicence && age > 17)
{
    Console.WriteLine("Can drive.");
}
else
{
    Console.WriteLine("Can't drive.");
}
*/


/*
* Vyvořte novou proměnnou typu bool a uložte do ní informaci, jestli má uživatel kreditku.
* Vypište na obrazovku, jestli si uživatel může nebo nemůže vzít půjčku
* Půjčku si může vzít člověk starší 18ti let s kreditkou
*/




/*
* Vypište na obrazovku, jestli si uživatel může nebo nemůže půjčit auto
* Auto si může půjčit ten, kdo může řídit a má kreditku
*/



/*
* Vypište na obrazovku, jestli se uživatel může prokázat.
* Prokázat se dá buď kreditkou nebo řidičákem.
*/


/*
* Vytvořte novou proměnnou typu string a uložte do ní nějaké heslo.
* Vypište na obrazovku, jestli heslo je nebo není validní.
* Validní heslo je dlouhé alespoň 8 znaků
*/

/*
* Následující kód nám představí nový datový typ "DayOfWeek".
*/
/*
DayOfWeek today = DateTime.Now.DayOfWeek;
Console.WriteLine(today.ToString());
if (today == DayOfWeek.Sunday)
{
Console.WriteLine("Let's go to church.");
}
*/

/*
* Vypište na obrazovku, jestli se dnes uživatel může dostat do Vídně.
* Do Vídně může přijet půjčeným autem nebo vlakem, který ale jezdí jen o víkendu (sobota a neděle)
*/

/*
 * Zkuste změnit původní hodnoty (např. věk) a otestujte svá řešení.
 */

// BONUS: rozšiřte validaci hesla tak, aby heslo obsahovalo alespoň jednu číslici (budete muset googlit)

// BONUS: Upravte podmínku s cestou do Vídně o časový údaj - vlak odjíždí ve 13:00. Tedy po 13té hodině nejde jet vlakem ani v sobotu ani v neděli.

