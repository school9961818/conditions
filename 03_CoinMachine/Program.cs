﻿int amount = 42;

/*
 * Nový (a lepší) automat na mince.
 * Napište program, který na obrazovku postupně vypíše nejmenší počet mincí,
 * na které se dá vstupní obnos (amount) rozměnit.
 * NOVĚ ale nebude vypisovat hodnoty, které nedostaneme (0x)
 * Mince jsou: 1, 2, 5, 10, 20, 50
 * Příklad:
 *   Dostanete:
 *   2x 20,
 *   1x 2,
 *   
 * Zkuste změnit obnos a ověřit funkčnost vašeho kódu.
 */
