﻿if (5 > 4)
{
    Console.WriteLine("Math and logic is valid.");
}

/*
 * Vidíte, že jednoduchá výjimka funguje.
 * Zkuste využít našeptávání Visual studia.
 * Napište "if" a počkejte, až vám vyskočí okno s nápovědou. Pak dejte dvakrát "Tab"
 */


/*
 * Povedlo se? Nyní napište jednoduchou podmínku, která ověří, že číslo 5 je rovno číslu 5.
 * (Nezapomeňte, že porovnání se dělá pomocí "==")
 */

/*
 * Veškerá porovnání najdete třeba zde - https://www.w3schools.com/cs/cs_operators_comparison.php
 * Nebo kdykoliv na google.
 * Úplnou podmínku uděláme tím, že za "if" blok dopíšeme "else" blok
 * Můžeme použít stejný našeptávač (napsat "else" a dvakrát Tab)
 * Následující kód zjistí, jestli je číslo liché ("%" je dělení se zbytkem)
 */

/*
int number = 7;
bool NumberIsOdd = (number % 2) == 1;
if (NumberIsOdd)
{
    Console.WriteLine($"Number {number} is odd.");
}
else
{
    Console.WriteLine($"Number {number} is even.");
}
*/

/*
 * Zkuste v předchozím bloku změnit číslo na sudé a ověřit funčnost.
 * Nyní vytvořte novou proměnnou int a dejte do ní hodnotu.
 * Vytvořte novou proměnnou typu bool, do které uložíte, jestli je číslo dělitelné třemi.
 * Na obrazovku vypište jestli je nebo není číslo dělitelné 3mi.
 */



/*
 * Projděte si následující odkaz - v tuto chvíli by mělo jít pouze o opakování
 * https://www.w3schools.com/cs/cs_conditions.php
 */