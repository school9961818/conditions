
## Prerequisites

[Variables](https://gitlab.com/school9961818/variables)

## Goal

* Code
  * Conditions
  * Logical operators
  * DayOfWeek

## Diffuculty

* Beginners
* 2-3x45 min

## Instructions

Podmínky umožňují algoritmům se rozhodovat. Např. provést akci jen v konkrétním  případě.
 ```mermaid
flowchart  TD  
Start --> IF{Condition}
IF --> |Yes| Action
Action --> Stop
IF --> |No| Stop
 ```

Pokud platí podmínka, provede se akce. Jinak se přeskočí. Tomuto se říká **neúplná podmínka**. V kódu vypadá takto
```c#
if(Condition)
{
  Action;
}
```

**Úplná podmínka** se používá, když chceme provést jedno nebo druhé.
 ```mermaid
flowchart  TD  
Start --> IF{Condition}
IF --> |Yes| Action_A
IF --> |No| Action_B
Action_A --> Stop
Action_B --> Stop
 ```

V kódu pak musíme přidat sekci `else`
```c#
if(Condition)
{
  Action_A;
}
else
{
  Action_B;
}
```

Naklonujte si projekt do počítače a postupně vyřešte zadání ve všech Projektech


## Feedback

https://forms.gle/Mv5Cmrm5Y6B41CiM6

## Where to next

[Cycles](https://gitlab.com/school9961818/cycles)
